package com.hudage.kotlinmvp.base

import android.content.Context
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

abstract class CommonAdapter<T>(var mContext: Context, var mData: ArrayList<T>, //条目布局
                                private var mLayoutId: Int)  {
}