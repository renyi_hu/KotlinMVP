package com.hudage.kotlinmvp.base

interface IBaseView {
    fun showLoading()
    fun dismissLoading()
}