package com.hudage.kotlinmvp.mvp.contract

import com.hudage.kotlinmvp.base.IBaseView
import com.hudage.kotlinmvp.base.IPresenter
import com.hudage.kotlinmvp.mvp.model.bean.DiscoveryBean

interface DiscoveryContract {
    interface View : IBaseView {
        /**
         * 显示分类的信息
         */
        fun showCategory(categoryList: ArrayList<DiscoveryBean>)

        /**
         * 显示错误信息
         */
        fun showError(errorMsg:String,errorCode:Int)
    }

    interface Presenter: IPresenter<View> {
        /**
         * 获取分类的信息
         */
        fun getDiscoveryData()
    }
}