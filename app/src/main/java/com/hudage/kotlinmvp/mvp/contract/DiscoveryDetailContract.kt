package com.hudage.kotlinmvp.mvp.contract

import com.hudage.kotlinmvp.base.IBaseView
import com.hudage.kotlinmvp.base.IPresenter
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean

interface DiscoveryDetailContract {
    interface View : IBaseView {
        /**
         *  设置列表数据
         */
        fun setCateDetailList(itemList: ArrayList<HomeBean.Issue.Item>)

        fun showError(errorMsg: String)
    }

    interface Presenter : IPresenter<View> {
        fun getCategoryDetailList(id: Long)
        fun loadMoreData()
    }
}