package com.hudage.kotlinmvp.mvp.model

import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.hudage.kotlinmvp.net.RetrofitManager
import com.hudage.kotlinmvp.rx.scheduler.SchedulerUtils
import io.reactivex.Observable

class DiscoveryDetailModel {
    /**
     * 获取分类信息
     */
    fun getDiscoveryDetailData(id: Long): Observable<HomeBean.Issue> {
        return RetrofitManager.service.getCategoryDetailList(id)
            .compose(SchedulerUtils.ioToMain())
    }

    /**
     * 拉取更多得数据
     */
    fun getMoreData(url: String): Observable<HomeBean.Issue> {
        return RetrofitManager.service.getIssueData(url)
            .compose(SchedulerUtils.ioToMain())
    }

}