package com.hudage.kotlinmvp.mvp.model

import com.hudage.kotlinmvp.mvp.model.bean.DiscoveryBean
import com.hudage.kotlinmvp.net.RetrofitManager
import com.hudage.kotlinmvp.rx.scheduler.SchedulerUtils
import io.reactivex.Observable

class DiscoveryModel {
    /**
     * 获取分类信息
     */
    fun getDiscoveryData(): Observable<ArrayList<DiscoveryBean>> {
        return RetrofitManager.service.getDiscovery()
            .compose(SchedulerUtils.ioToMain())
    }
}