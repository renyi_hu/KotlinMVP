package com.hudage.kotlinmvp.mvp.model

import android.util.Log
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.hudage.kotlinmvp.net.RetrofitManager
import com.hudage.kotlinmvp.rx.scheduler.SchedulerUtils
import io.reactivex.Observable

class HomeModel {

    /**
     *获取首页 Banner 数据
     */
    fun requestHomeData(num: Int): Observable<HomeBean> {
        return RetrofitManager.service.getFirstHomeData(num)
            .compose(SchedulerUtils.ioToMain())
    }

    /**
     * 加载更多
     */
    fun loadMoreData(url: String): Observable<HomeBean> {
        return RetrofitManager.service.getMoreHomeData(url)
            .compose(SchedulerUtils.ioToMain())
    }
}