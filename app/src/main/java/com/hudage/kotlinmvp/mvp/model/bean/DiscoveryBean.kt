package com.hudage.kotlinmvp.mvp.model.bean

import java.io.Serializable

data class DiscoveryBean(val id: Long, val name: String, val description: String,
                         val bgPicture: String, val bgColor: String, val headerImage: String):Serializable