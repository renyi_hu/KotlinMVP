package com.hudage.kotlinmvp.mvp.model.bean

import cn.bmob.v3.BmobObject

class PersonBean : BmobObject() {
    var name: String? = null
    var age: String? = null
}