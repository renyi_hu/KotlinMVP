package com.hudage.kotlinmvp.mvp.presenter

import com.hudage.kotlinmvp.base.BasePresenter
import com.hudage.kotlinmvp.mvp.contract.DiscoveryContract
import com.hudage.kotlinmvp.mvp.model.DiscoveryModel
import com.hudage.kotlinmvp.net.ExceptionHandle

class DiscoveryPresenter : BasePresenter<DiscoveryContract.View>(), DiscoveryContract.Presenter {

    private val discoveryModel: DiscoveryModel by lazy {
        DiscoveryModel()
    }

    /**
     * 抓取分类
     */
    override fun getDiscoveryData() {
        checkViewAttached()
        mRootView?.showLoading()
        val disposable = discoveryModel.getDiscoveryData()
            .subscribe({ categoryList ->
                mRootView?.apply {
                    dismissLoading()
                    showCategory(categoryList)
                }
            }, { t ->
                mRootView?.apply {
                    //处理异常
                    showError(ExceptionHandle.handleException(t),ExceptionHandle.errorCode)
                }

            })

        addSubscription(disposable)
    }

}