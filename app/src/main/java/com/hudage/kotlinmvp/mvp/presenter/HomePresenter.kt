package com.hudage.kotlinmvp.mvp.presenter

import com.hudage.kotlinmvp.base.BasePresenter
import com.hudage.kotlinmvp.mvp.contract.HomeContract
import com.hudage.kotlinmvp.mvp.model.HomeModel
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.hudage.kotlinmvp.net.ExceptionHandle
import com.orhanobut.logger.Logger

class HomePresenter : BasePresenter<HomeContract.V>(), HomeContract.Presenter {

    private val TAG: String = "HomePresenter"

    private var bannerHomeBean: HomeBean? = null
    private var nextPageUrl: String? = null

    private val homeModel: HomeModel by lazy {
        HomeModel()
    }


    override fun requestHomeData(num: Int) {
        //检测是否已经绑定View
        checkViewAttached()
        mRootView?.showLoading()
        val disposable = homeModel.requestHomeData(num)
            .flatMap { homeBean ->
                val bannerItemList = homeBean.issueList[0].itemList
                bannerItemList.filter { item -> item.type == "banner2" || item.type == "horizontalScrollCard" }
                    .forEach { it -> bannerItemList.remove(it) }
                bannerHomeBean = homeBean
                //根据 nextPageUrl 请求下一页数据
                homeModel.loadMoreData(homeBean.nextPageUrl)
            }
            .subscribe({ homeBean ->
                mRootView?.apply {
                    dismissLoading()
                    nextPageUrl = homeBean.nextPageUrl
                    //过滤掉 Banner2(包含广告,等不需要的 Type), 具体查看接口分析
                    val newBannerItemList = homeBean.issueList[0].itemList
                    newBannerItemList.filter { item ->
                        item.type == "banner2" || item.type == "horizontalScrollCard" || item.type == "textHeader"
                    }.forEach { item ->
                        //移除 item
                        newBannerItemList.remove(item)
                    }
                    // 重新赋值 Banner 长度
                    bannerHomeBean!!.issueList[0].count =
                        bannerHomeBean!!.issueList[0].itemList.size

                    //赋值过滤后的数据 + banner 数据
                    bannerHomeBean?.issueList!![0].itemList.addAll(newBannerItemList)
                    setHomeBannerData(bannerHomeBean!!)
                }
            }, { t ->
                mRootView?.apply {
                    dismissLoading()
                    showError(ExceptionHandle.handleException(t), ExceptionHandle.errorCode)
                }
            })
        addSubscription(disposable)
    }

    override fun loadMoreData() {
        val disposable = nextPageUrl?.let {
            homeModel.loadMoreData(it)
                .subscribe({ homeBean ->
                    mRootView?.apply {
                        //过滤掉 Banner2(包含广告,等不需要的 Type), 具体查看接口分析
                        val newItemList = homeBean.issueList[0].itemList
                        newItemList.filter { item ->
                            item.type == "banner2" || item.type == "horizontalScrollCard" || item.type == "textHeader"
                        }.forEach { item ->
                            //移除 item
                            newItemList.remove(item)
                        }
                        Logger.d( homeBean.nextPageUrl)
                        nextPageUrl = homeBean.nextPageUrl
                        setMoreData(newItemList)
                    }

                }, { t ->
                    mRootView?.apply {
                        showError(ExceptionHandle.handleException(t), ExceptionHandle.errorCode)
                    }
                })


        }
        if (disposable != null) {
            addSubscription(disposable)
        }
    }
}