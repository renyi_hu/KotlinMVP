package com.hudage.kotlinmvp.ui

import android.view.View
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseActivity
import com.hudage.kotlinmvp.mvp.model.bean.PersonBean
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {


    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun initView() {
        setUpNavBottom()
    }

    override fun initData() {
//        val personBean = PersonBean()
//        personBean.name = "hudadage"
//        personBean.age = "nihaoa"
//        personBean.save(object : SaveListener<String>() {
//            override fun done(p0: String?, p1: BmobException?) {
//                if (p1 == null) {
//                    Toast.makeText(this@MainActivity, "添加数据成功：$p0", Toast.LENGTH_SHORT).show()
//                } else {
//                    Toast.makeText(this@MainActivity, "添加数据失败：$p1", Toast.LENGTH_SHORT).show()
//                }
//            }
//
//        })
    }

    override fun start() {
    }

    private fun setUpNavBottom() {
        val hostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment) as NavHostFragment
        val navMenu: BottomNavigationView = this.findViewById(R.id.bottom_navigation_view)
        val navController: NavController = hostFragment.navController
        NavigationUI.setupWithNavController(navMenu, navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.emptyFragment) {
                bottom_navigation_view.visibility = View.GONE
            } else {
                bottom_navigation_view.visibility = View.VISIBLE
            }
        }
    }
}
