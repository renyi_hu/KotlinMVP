package com.hudage.kotlinmvp.ui

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseActivity
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.utils.OrientationUtils
import kotlinx.android.synthetic.main.activity_simple_player.*

class SimplePlayer : BaseActivity() {

    lateinit var orientationUtils: OrientationUtils

    override fun layoutId(): Int {
        return R.layout.activity_simple_player
    }

    override fun initView() {

    }

    override fun initData() {
        val intent = intent
        val source = intent.getStringExtra("url")
        val title = intent.getStringExtra("title")
        gsy_player.setUp(source, true, title)

        val imageView = ImageView(this)
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        imageView.setImageResource(R.mipmap.img_avatar)
        gsy_player.thumbImageView = imageView
        gsy_player.titleTextView.visibility = View.VISIBLE
        gsy_player.backButton.visibility = View.VISIBLE
        orientationUtils = OrientationUtils(this, gsy_player)
        gsy_player.fullscreenButton.setOnClickListener {
            orientationUtils.resolveByClick()
        }
        gsy_player.setIsTouchWiget(true)
        gsy_player.backButton.setOnClickListener {
            onBackPressed()
        }
        gsy_player.startPlayLogic()

    }

    override fun start() {
    }

    override fun onPause() {
        super.onPause()
        gsy_player.onVideoPause()
    }

    override fun onResume() {
        super.onResume()
        gsy_player.onVideoResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        GSYVideoManager.releaseAllVideos()
        if (orientationUtils != null) {
            orientationUtils.releaseListener()
        }
    }

    override fun onBackPressed() {
        if (orientationUtils.screenType == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            gsy_player.fullscreenButton.performClick()
            return
        }
        gsy_player.setVideoAllCallBack(null)
        super.onBackPressed()
    }
}