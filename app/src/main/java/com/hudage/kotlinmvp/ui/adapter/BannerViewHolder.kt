package com.hudage.kotlinmvp.ui.adapter

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hudage.kotlinmvp.MyApplication
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.orhanobut.logger.Logger
import com.zhpan.bannerview.holder.ViewHolder

class BannerViewHolder : ViewHolder<HomeBean.Issue.Item> {
    override fun getLayoutId(): Int {
        return R.layout.banner
    }

    override fun onBind(itemView: View?, data: HomeBean.Issue.Item?, position: Int, size: Int) {
        val ivCover = itemView?.findViewById<ImageView>(R.id.banner_image)
        if (null != data?.data?.cover) {
            Glide.with(MyApplication.context).load(data.data.cover.feed)
                .into(ivCover!!)
        } else {
            ivCover!!.setImageResource(R.mipmap.img_avatar)
        }
        val tvVideo= itemView.findViewById<TextView>(R.id.tv_describe)
        if (null!=data?.data?.title){
            tvVideo.text = data.data.title
        }else{
            tvVideo.text = "there is no title"
        }
    }

}