package com.hudage.kotlinmvp.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.hudage.kotlinmvp.MyApplication
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.durationFormat
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean

class HomeAdapter(layoutResId: Int, data: ArrayList<HomeBean.Issue.Item>) :
    BaseQuickAdapter<HomeBean.Issue.Item, BaseViewHolder>(layoutResId, data) {
    override fun convert(helper: BaseViewHolder, item: HomeBean.Issue.Item?) {
        var tagText: String? = "#"
        //遍历标签
        item?.data?.tags?.take(4)?.forEach {
            tagText += (it.name + "/")
        }
        // 格式化时间
        val timeFormat = durationFormat(item?.data?.duration)
        if (null != item?.data?.cover?.feed) {
            Glide.with(MyApplication.context).load(item.data.cover.feed)
                .into(helper.getView(R.id.iv_cover_feed))
        } else {
            helper.setImageResource(R.id.iv_cover_feed, R.mipmap.img_avatar)
        }

        if (null != item?.data?.author?.icon) {
            Glide.with(MyApplication.context).load(item.data.author.icon)
                .into(helper.getView(R.id.civ_avatar))
        } else {
            helper.setImageResource(R.id.civ_avatar, R.mipmap.img_avatar)
        }
        helper.setText(R.id.tv_title, item?.data?.title)
        helper.setText(R.id.tv_tag, tagText)

        helper.addOnClickListener(R.id.iv_cover_feed)
        helper.addOnClickListener(R.id.civ_avatar)
    }
}

