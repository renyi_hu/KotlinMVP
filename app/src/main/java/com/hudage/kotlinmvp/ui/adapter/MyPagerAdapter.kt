package com.hudage.kotlinmvp.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MyPagerAdapter :
    FragmentPagerAdapter {
    private lateinit var fragments: List<Fragment>

    constructor(fragmentManager: FragmentManager, behavior: Int, fragments: List<Fragment>) :
            super(fragmentManager, behavior) {
        this.fragments = fragments
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }
}