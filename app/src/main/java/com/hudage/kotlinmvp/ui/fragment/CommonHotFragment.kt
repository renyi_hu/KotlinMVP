package com.hudage.kotlinmvp.ui.fragment

import android.content.Intent
import android.view.LayoutInflater
import android.widget.AdapterView
import com.google.gson.Gson
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import com.hudage.kotlinmvp.mvp.model.bean.HotStraetgyBean
import com.hudage.kotlinmvp.ui.SimplePlayer
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem
import com.orhanobut.logger.Logger
import com.zhy.http.okhttp.OkHttpUtils
import com.zhy.http.okhttp.callback.StringCallback
import kotlinx.android.synthetic.main.fragment_common_hot_fragment.*

class CommonHotFragment : BaseFragment() {


    val itemListEntities: MutableList<HotStraetgyBean.ItemListEntity> = ArrayList()
    override fun getLayoutId(): Int {
        return R.layout.fragment_common_hot_fragment
    }

    override fun initView() {
        val position = FragmentPagerItem.getPosition(arguments)
        Logger.d("---->position + $position")
        var stretary = STRATEGY[position]
        var url = String.format(
            "http://baobab.kaiyanapp.com/api/v3/ranklist?num=10&strategy=%s&udid=26868b32e808498db32fd51fb422d00175e179df&vc=83",
            stretary
        )
        OkHttpUtils.get().url(url).build().execute(object : StringCallback() {
            override fun onResponse(response: String, id: Int) {
                pareJson(response)
            }

            override fun onError(call: okhttp3.Call?, e: Exception?, id: Int) {}
        })

        //添加底部布局
        val view = LayoutInflater.from(context).inflate(R.layout.list_footer, null)
        hot_listview.addFooterView(view)

        setListener()
    }

    private fun setListener() {
        hot_listview.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val data = itemListEntities[position].data
                val intent = Intent(context, SimplePlayer::class.java)
                intent.putExtra("url", data?.playUrl)
                intent.putExtra("title", data?.title)
                startActivity(intent)
            }
    }

    fun pareJson(jsonData: String) {
        val hotStraetgyBean: HotStraetgyBean =
            Gson().fromJson(jsonData, HotStraetgyBean::class.java)
        hotStraetgyBean.itemList?.let { itemListEntities.addAll(it) }
        setAdapter(itemListEntities)
    }

    companion object {
        private val STRATEGY = arrayOf("weekly", "monthly", "historical")
    }

    fun setAdapter(itemListEntities: MutableList<HotStraetgyBean.ItemListEntity>) {
    }
}