package com.hudage.kotlinmvp.ui.fragment

import android.content.Intent
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.hudage.kotlinmvp.MyApplication
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import com.hudage.kotlinmvp.mvp.contract.DiscoveryDetailContract
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.hudage.kotlinmvp.mvp.presenter.DiscoveryDetailPresenter
import com.hudage.kotlinmvp.ui.SimplePlayer
import com.hudage.kotlinmvp.ui.adapter.HomeAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_follow_layout.*

/**
 * 该界面最初是将recyclerView放置到XML文件中，但是在initView时总是会layout skipped
 * 从而导致RecyclerView无法加载得问题
 * 因此采用动态添加的方式，在onResume之后将RecyclerView添加至container
 */
class DiscoveryDetailsFragment(var discoveryId: Long) : BaseFragment(),
    DiscoveryDetailContract.View, BaseQuickAdapter.RequestLoadMoreListener {

    private lateinit var recyclerViewDiscoveryDetails: RecyclerView
    private lateinit var discoveryDetailsAdapter: HomeAdapter
    private lateinit var itemAllList: ArrayList<HomeBean.Issue.Item>
    private lateinit var mContainer: LinearLayout


    private val mPresenter by lazy {
        DiscoveryDetailPresenter()
    }


    override fun getLayoutId(): Int = R.layout.fragment_follow_layout

    override fun initView() {
        mPresenter.attachView(this)
        mContainer = container
    }

    override fun onResume() {
        super.onResume()
        recyclerViewDiscoveryDetails = RecyclerView(MyApplication.context)
        itemAllList = ArrayList()
        initDiscoveryAdapter(itemAllList)
        mContainer.addView(recyclerViewDiscoveryDetails)
        mPresenter.getCategoryDetailList(discoveryId)
    }

    override fun setCateDetailList(itemList: ArrayList<HomeBean.Issue.Item>) {
        itemList.forEach { item ->
            itemAllList.forEach {
                if (it.data!!.id != item.data!!.id) {
                    itemAllList.add(item)
                } else {
                    itemList.remove(item)
                }
            }
        }
        if (itemAllList.size < 5) {
            onLoadMoreRequested()
        }
        discoveryDetailsAdapter.addData(itemList)
    }

    override fun showError(errorMsg: String) {
    }

    override fun showLoading() {
    }

    override fun dismissLoading() {
    }

    private fun initDiscoveryAdapter(itemList: ArrayList<HomeBean.Issue.Item>) {
        discoveryDetailsAdapter = HomeAdapter(R.layout.home_page_video_card, itemList)
        discoveryDetailsAdapter.setOnLoadMoreListener(this, recyclerViewDiscoveryDetails)
        discoveryDetailsAdapter.setEnableLoadMore(true)
        discoveryDetailsAdapter.setOnItemChildClickListener { _, view, position ->
            when (view.id) {
                R.id.civ_avatar, R.id.iv_cover_feed -> {
                    Logger.d("$position --- ${itemAllList[position]} ")
                    val intent = Intent(activity, SimplePlayer::class.java)
                    intent.putExtra("url", itemAllList[position].data?.playUrl)
                    intent.putExtra("title", itemAllList[position].data?.text)
                    startActivity(intent)
                }
            }
        }
        recyclerViewDiscoveryDetails.adapter = discoveryDetailsAdapter
        recyclerViewDiscoveryDetails.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
    }

    override fun onLoadMoreRequested() {
        mPresenter.loadMoreData()
        recyclerViewDiscoveryDetails.postDelayed(Runnable {
            discoveryDetailsAdapter.loadMoreComplete()
        }, 500)
    }
}