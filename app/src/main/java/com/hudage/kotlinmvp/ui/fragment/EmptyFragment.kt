package com.hudage.kotlinmvp.ui.fragment


import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_empty.*

/**
 * A simple [Fragment] subclass.
 */
class EmptyFragment() : BaseFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_empty

    override fun initView() {
        btn_click_test.setOnClickListener {
            Navigation.findNavController(btn_click_test)
                .navigate(R.id.action_emptyFragment2_to_mineFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        Logger.d("EmptyFragment has start Resume")
    }
}
