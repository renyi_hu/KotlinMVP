package com.hudage.kotlinmvp.ui.fragment


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import com.hudage.kotlinmvp.mvp.contract.HomeContract
import com.hudage.kotlinmvp.mvp.model.bean.HomeBean
import com.hudage.kotlinmvp.mvp.presenter.HomePresenter
import com.hudage.kotlinmvp.ui.SimplePlayer
import com.hudage.kotlinmvp.ui.adapter.BannerViewHolder
import com.hudage.kotlinmvp.ui.adapter.HomeAdapter
import com.orhanobut.logger.Logger
import com.zhpan.bannerview.BannerViewPager
import com.zhpan.bannerview.adapter.OnPageChangeListenerAdapter
import com.zhpan.bannerview.constants.IndicatorGravity
import com.zhpan.bannerview.constants.IndicatorSlideMode


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment(), HomeContract.V, BaseQuickAdapter.RequestLoadMoreListener {

    private val mPresenter by lazy { HomePresenter() }

    //视频列表
    private lateinit var mHomeRecyclerView: RecyclerView
    private lateinit var mHomeAdapter: HomeAdapter

    //banner列表
    private lateinit var mViewpager: BannerViewPager<HomeBean.Issue.Item, BannerViewHolder>
    private lateinit var myView: View
    private var isFirstLoad: Boolean = true

    private lateinit var itemAllList: ArrayList<HomeBean.Issue.Item>
    private val linearLayoutManager by lazy {
        LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
//        StaggeredGridLayoutManager(2,LinearLayoutManager.VERTICAL)
    }


    companion object {
        fun getInstance(): HomeFragment {
            val fragment = HomeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun initView() {
        mPresenter.attachView(this)
        mPresenter.requestHomeData(1)
        mHomeRecyclerView = activity!!.findViewById(R.id.rv_home_page_video)
        //banner
        myView = View.inflate(activity, R.layout.banner_view_pager, null)
        mViewpager = myView.findViewById(R.id.banner_view_pager)

        itemAllList = ArrayList()
    }

    override fun setHomeBannerData(homeBean: HomeBean) {
        initBannerView(homeBean)
        mPresenter.loadMoreData()
    }

    override fun setMoreData(itemList: ArrayList<HomeBean.Issue.Item>) {
        if (isFirstLoad) {
            itemList.forEach {
                itemAllList.add(it)
            }
            initHomeAdapter(itemList)
            isFirstLoad = false
        } else {
            itemList.forEach {
                itemAllList.add(it)
            }
            mHomeAdapter.addData(itemList)
        }
    }

    override fun showError(msg: String, errorCode: Int) {
    }

    override fun showLoading() {
    }

    override fun dismissLoading() {
    }

    private fun initBannerView(homeBean: HomeBean) {
        mViewpager.setCanLoop(false)
            .setIndicatorSlideMode(IndicatorSlideMode.SMOOTH)
            .setIndicatorMargin(0, 0, 0, 0)
            .setIndicatorGravity(IndicatorGravity.CENTER)
            .setHolderCreator { BannerViewHolder() }
            .setAutoPlay(true)
            .setCanLoop(true)
            .setOnPageChangeListener(
                object : OnPageChangeListenerAdapter() {
                    override fun onPageSelected(position: Int) {
                    }
                }
            )
            .create(homeBean.issueList[0].itemList)
    }

    private fun initHomeAdapter(itemList: ArrayList<HomeBean.Issue.Item>) {
        mHomeAdapter = HomeAdapter(R.layout.home_page_video_card, itemList)
        mHomeAdapter.addHeaderView(myView)
        mHomeAdapter.setOnLoadMoreListener(this, mHomeRecyclerView)
        mHomeAdapter.setEnableLoadMore(true)
        mHomeAdapter.setOnItemChildClickListener { _, view, position ->
            when (view.id) {
                R.id.civ_avatar, R.id.iv_cover_feed -> {
                    Logger.d("$position --- ${itemAllList[position]} ")
                    val intent = Intent(activity, SimplePlayer::class.java)
                    intent.putExtra("url", itemAllList[position].data?.playUrl)
                    intent.putExtra("title", itemAllList[position].data?.text)
                    startActivity(intent)
                }
            }
        }
        mHomeRecyclerView.adapter = mHomeAdapter
        mHomeRecyclerView.layoutManager = linearLayoutManager
    }

    override fun onLoadMoreRequested() {
        mPresenter.loadMoreData()
        mHomeRecyclerView.postDelayed(Runnable {
            mHomeAdapter.loadMoreComplete()
        }, 1000)
    }
}






