package com.hudage.kotlinmvp.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import com.hudage.kotlinmvp.ui.adapter.MyPagerAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import kotlinx.android.synthetic.main.fragment_hot_layout.*

class HotFragment : BaseFragment() {

    var fragments = ArrayList<Fragment>()

    companion object {
        fun getInstance(): HotFragment {
            val fragment = HotFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    val TITLE = arrayOf("周排行", "月排行", "总排行")

    override fun getLayoutId(): Int = R.layout.fragment_hot_layout

    override fun initView() {
        TITLE.forEach {
            val commonHotFragment = CommonHotFragment()
            fragments.add(commonHotFragment)
        }
        hot_viewpager.offscreenPageLimit = 3

        val adapter = FragmentPagerItemAdapter(
            childFragmentManager, FragmentPagerItems.with(context)
                .add(TITLE[0], fragments[0].javaClass)
                .add(TITLE[1], fragments[1].javaClass)
                .add(TITLE[2], fragments[2].javaClass)
                .create()
        )
        hot_viewpager.adapter = adapter
        viewpagertab.setViewPager(hot_viewpager)

    }
}