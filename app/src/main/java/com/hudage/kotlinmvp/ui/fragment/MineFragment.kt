package com.hudage.kotlinmvp.ui.fragment

import android.os.Bundle
import android.view.View
import com.hudage.kotlinmvp.R
import com.hudage.kotlinmvp.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_mine_layout.*

class MineFragment : BaseFragment(), View.OnClickListener {
    companion object {
        fun getInstance(): MineFragment {
            val fragment = MineFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_mine_layout

    override fun initView() {
        civ_user_avatar.setOnClickListener(this)
        tv_view_homepage.setOnClickListener(this)
        ll_collection.setOnClickListener(this)
        ll_comment.setOnClickListener(this)
        tv_mine_message.setOnClickListener(this)
        tv_mine_attention.setOnClickListener(this)
        tv_mine_cache.setOnClickListener(this)
        tv_watch_history.setOnClickListener(this)
        tv_feedback.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
    }
}